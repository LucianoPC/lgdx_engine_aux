package lgdx_engine_aux.handlers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;

public class FontHandler {

	public static final int SIZE_10 = 10;
	public static final int SUPER_SMALL_SIZE = 14;
	public static final int SMALL_SIZE = 28;
	public static final int MEDIUM_SIZE = 72;

	private static final String FONT_PATH = "font/republica_minor_regular.ttf";

	private static FontHandler instance;

	public static FontHandler getInstance() {
		if (instance == null) {
			instance = new FontHandler();
		}
		return instance;
	}

	private BitmapFont bitmapFontSize10;
	private BitmapFont bitmapFontSuperSmall;
	private BitmapFont bitmapFontSmall;
	private BitmapFont bitmapFontMedium;

	private FontHandler() {
		loadResources();
	}

	public BitmapFont getBitmapFont(int size) {
		if (size == SIZE_10) {
			return getBitmapFontSize10();
		} else if (size == SUPER_SMALL_SIZE) {
			return getBitmapFontSuperSmallSize();
		} else if (size == SMALL_SIZE) {
			return getBitmapFontSmallSize();
		} else {
			return getBitmapFontMediumSize();
		}
	}

	public BitmapFont getBitmapFontMediumSize() {
		this.bitmapFontMedium.getCache()
				.setColor(ThemeHandler.getStrongColor());
		return this.bitmapFontMedium;
	}

	public BitmapFont getBitmapFontSmallSize() {
		this.bitmapFontSmall.getCache().setColor(Color.BLACK);
		return this.bitmapFontSmall;
	}

	public BitmapFont getBitmapFontSuperSmallSize() {
		this.bitmapFontSuperSmall.getCache().setColor(Color.BLACK);
		return this.bitmapFontSuperSmall;
	}
	
	public BitmapFont getBitmapFontSize10() {
		this.bitmapFontSize10.getCache().setColor(
				ThemeHandler.getStrongColor());
		return this.bitmapFontSize10;
	}

	public void loadResources() {
		FreeTypeFontGenerator fontGenerator = new FreeTypeFontGenerator(
				Gdx.files.internal(FONT_PATH));
		FreeTypeFontParameter fontParameter = new FreeTypeFontParameter();

		fontParameter.size = SMALL_SIZE;
		this.bitmapFontSmall = fontGenerator.generateFont(fontParameter);

		fontParameter.size = MEDIUM_SIZE;
		this.bitmapFontMedium = fontGenerator.generateFont(fontParameter);

		fontParameter.size = SUPER_SMALL_SIZE;
		this.bitmapFontSuperSmall = fontGenerator.generateFont(fontParameter);

		fontParameter.size = SIZE_10;
		this.bitmapFontSize10 = fontGenerator.generateFont(fontParameter);
	}
}

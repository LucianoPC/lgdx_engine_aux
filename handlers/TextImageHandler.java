package lgdx_engine_aux.handlers;

import java.util.TreeMap;

import lgdx_engine_aux.basic.LImage;

public class TextImageHandler {

	public static TextImageHandler instance;
	
	public static TextImageHandler getInstance() {
		if(instance == null) {
			instance = new TextImageHandler();
		}
		return instance;
	}
	
	private TreeMap<String, LImage> imageMap;
	
	private TextImageHandler() {
		loadResources();
	}
	
	public LImage getImage(String prefix, String textChar) {
		String key = prefix + "_" + textChar;
		if(!this.imageMap.containsKey(textChar)) {
			LImage image = new LImage(key + ".png");
			this.imageMap.put(key, image);
		}
		return this.imageMap.get(key);
	}
	
	public void loadResources() {
		this.imageMap = new TreeMap<String, LImage>();
	}
}

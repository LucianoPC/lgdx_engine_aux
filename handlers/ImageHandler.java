package lgdx_engine_aux.handlers;

import lgdx_engine_aux.util.ScreenParams;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class ImageHandler {

	private static ImageHandler instance;

	public static ImageHandler getInstance() {
		if (instance == null) {
			instance = new ImageHandler();
		}
		return instance;
	}

	public static TextureRegion load(String imagePath) {
		return getInstance().loadTextureAtlas(imagePath);
	}

	private String imageFolder;
	private TextureAtlas textureAtlas;

	private ImageHandler() {
		loadResources();
	}

	public void loadResources() {
		this.imageFolder = ScreenParams.getInstance().imageFolder;
		String packPath = "data/pack_" + this.imageFolder
				+ "/crayon_click_pack.pack";
		this.textureAtlas = new TextureAtlas(packPath);
	}

	public TextureRegion loadTextureAtlas(String imagePath) {
		imagePath = imagePath.substring(0, imagePath.length() - 4);
		TextureRegion textureRegion = new TextureRegion(
				this.textureAtlas.findRegion(imagePath));

		return textureRegion;
	}
}

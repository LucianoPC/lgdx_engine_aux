package lgdx_engine_aux.handlers;

import lgdx_engine_aux.basic.Positionable;

public class PositionHandler {

	public static float getLeft(Positionable positionable) {
		return positionable.getPosition().x - (positionable.getWidth() / 2.0f);
	}

	public static float getRight(Positionable positionable) {
		return positionable.getPosition().x + (positionable.getWidth() / 2.0f);
	}

	public static float getTop(Positionable positionable) {
		return positionable.getPosition().y + (positionable.getHeight() / 2.0f);
	}

	public static float getBottom(Positionable positionable) {
		return positionable.getPosition().y - (positionable.getHeight() / 2.0f);
	}

	public static void alignCenter(Positionable reference,
			Positionable positionable) {
		float x = reference.getPosition().x;
		float y = reference.getPosition().y;

		positionable.getPosition().set(x, y);
	}

	public static void alignCenterHorizontal(Positionable reference,
			Positionable positionable) {
		float x = reference.getPosition().x;
		float y = positionable.getPosition().y;

		positionable.getPosition().set(x, y);
	}

	public static void alignCenterVertical(Positionable reference,
			Positionable positionable) {
		float x = positionable.getPosition().x;
		float y = reference.getPosition().y;

		positionable.getPosition().set(x, y);
	}

	public static void alignCenterToTop(Positionable reference,
			Positionable positionable) {
		alignCenterToTop(reference, positionable, 0.0f);
	}

	public static void alignCenterToTop(Positionable reference,
			Positionable positionable, float distance) {
		float x = positionable.getPosition().x;
		float y = reference.getPosition().y + (reference.getHeight() / 2.0f);
		y += distance;

		positionable.getPosition().set(x, y);
	}

	public static void alignLeftToLeft(Positionable reference,
			Positionable positionable) {
		alignLeftToLeft(reference, positionable, 0.0f);
	}

	public static void alignLeftToLeft(Positionable reference,
			Positionable positionable, float distance) {
		float x = reference.getPosition().x - (reference.getWidth() / 2.0f);
		x += positionable.getWidth() / 2.0f;
		x += distance;

		float y = positionable.getPosition().y;

		positionable.getPosition().set(x, y);
	}

	public static void alignLeftToRight(Positionable reference,
			Positionable positionable) {
		alignLeftToRight(reference, positionable, 0.0f);
	}

	public static void alignLeftToRight(Positionable reference,
			Positionable positionable, float distance) {
		float x = reference.getPosition().x + (reference.getWidth() / 2.0f);
		x += positionable.getWidth() / 2.0f;
		x += distance;

		float y = positionable.getPosition().y;

		positionable.getPosition().set(x, y);
	}

	public static void alignRightToRight(Positionable reference,
			Positionable positionable) {
		alignRightToRight(reference, positionable, 0.0f);
	}

	public static void alignRightToRight(Positionable reference,
			Positionable positionable, float distance) {
		float x = reference.getPosition().x + (reference.getWidth() / 2.0f);
		x -= positionable.getWidth() / 2.0f;
		x -= distance;

		float y = positionable.getPosition().y;

		positionable.getPosition().set(x, y);
	}

	public static void alignRightToLeft(Positionable reference,
			Positionable positionable) {
		alignRightToLeft(reference, positionable, 0.0f);
	}

	public static void alignRightToLeft(Positionable reference,
			Positionable positionable, float distance) {
		float x = reference.getPosition().x - (reference.getWidth() / 2.0f);
		x -= positionable.getWidth() / 2.0f;
		x -= distance;

		float y = positionable.getPosition().y;

		positionable.getPosition().set(x, y);
	}

	public static void alignBottomToBottom(Positionable reference,
			Positionable positionable) {
		alignBottomToBottom(reference, positionable, 0.0f);
	}

	public static void alignBottomToBottom(Positionable reference,
			Positionable positionable, float distance) {
		float x = positionable.getPosition().x;

		float y = reference.getPosition().y - (reference.getHeight() / 2.0f);
		y += positionable.getHeight() / 2.0f;
		y += distance;

		positionable.getPosition().set(x, y);
	}

	public static void alignBottomToTop(Positionable reference,
			Positionable positionable) {
		alignBottomToTop(reference, positionable, 0.0f);
	}

	public static void alignBottomToTop(Positionable reference,
			Positionable positionable, float distance) {
		float x = positionable.getPosition().x;

		float y = reference.getPosition().y + (reference.getHeight() / 2.0f);
		y += positionable.getHeight() / 2.0f;
		y += distance;

		positionable.getPosition().set(x, y);
	}

	public static void alignTopToTop(Positionable reference,
			Positionable positionable) {
		alignTopToTop(reference, positionable, 0.0f);
	}

	public static void alignTopToTop(Positionable reference,
			Positionable positionable, float distance) {
		float x = positionable.getPosition().x;

		float y = reference.getPosition().y + (reference.getHeight() / 2.0f);
		y -= positionable.getHeight() / 2.0f;
		y -= distance;

		positionable.getPosition().set(x, y);
	}

	public static void alignTopToBottom(Positionable reference,
			Positionable positionable) {
		alignTopToBottom(reference, positionable, 0.0f);
	}

	public static void alignTopToBottom(Positionable reference,
			Positionable positionable, float distance) {
		float x = positionable.getPosition().x;

		float y = reference.getPosition().y - (reference.getHeight() / 2.0f);
		y -= positionable.getHeight() / 2.0f;
		y -= distance;

		positionable.getPosition().set(x, y);
	}

	public static void alignBetweenVertical(Positionable top, Positionable bottom,
			Positionable positionable) {
		float x = positionable.getPosition().x;
		float y = (getBottom(top) + getTop(bottom)) / 2.0f;

		positionable.getPosition().set(x, y);
	}
}

package lgdx_engine_aux.handlers;

import java.util.ArrayList;
import java.util.Random;

import lgdx_engine_aux.basic.GameTheme;

import com.badlogic.gdx.graphics.Color;

public class ThemeHandler {

	private static ThemeHandler instance;

	public static ThemeHandler getInstance() {
		if (instance == null) {
			instance = new ThemeHandler();
		}
		return instance;
	}

	private int themeIndex;
	private Random random;

	private ArrayList<GameTheme> gameThemeList;

	private ThemeHandler() {
		this.themeIndex = 0;

		this.random = new Random();

		this.gameThemeList = new ArrayList<GameTheme>();
	}

	public static void sort() {
		ThemeHandler.getInstance().sortTheme();
	}
	
	public void add(GameTheme gameTheme) {
		this.gameThemeList.add(gameTheme);
	}

	public void sortTheme() {
		int nextIndex = this.themeIndex;

		while ((nextIndex == this.themeIndex)
				&& (this.gameThemeList.size() > 1)) {
			nextIndex = this.random.nextInt(this.gameThemeList.size());
		}

		this.themeIndex = nextIndex;
	}

	public static ArrayList<GameTheme> getGameThemes() {
		return ThemeHandler.getInstance().getGameThemeList();
	}

	public ArrayList<GameTheme> getGameThemeList() {
		return this.gameThemeList;
	}

	public static String getThemedPath(String imageName) {
		return getFolder() + imageName;
	}

	public static String getFolder() {
		return ThemeHandler.getInstance().getThemeFolder();
	}

	public String getThemeFolder() {
		return this.gameThemeList.get(this.themeIndex).getFolder();
	}

	public static Color getLightColor() {
		return ThemeHandler.getInstance().getThemeLightColor();
	}

	public Color getThemeLightColor() {
		return this.gameThemeList.get(this.themeIndex).getLightColor();
	}

	public static Color getMediumColor() {
		return ThemeHandler.getInstance().getThemeMediumColor();
	}

	public Color getThemeMediumColor() {
		return this.gameThemeList.get(this.themeIndex).getMediumColor();
	}

	public static Color getStrongColor() {
		return ThemeHandler.getInstance().getThemeStrongColor();
	}

	public Color getThemeStrongColor() {
		return this.gameThemeList.get(this.themeIndex).getStrongColor();
	}
}

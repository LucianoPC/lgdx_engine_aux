package lgdx_engine_aux.handlers;

import java.util.TreeMap;

import lgdx_engine_aux.basic.LImage;

public class NumberImageHandler {

	public static NumberImageHandler instance;
	
	public static NumberImageHandler getInstance() {
		if(instance == null) {
			instance = new NumberImageHandler();
		}
		return instance;
	}
	
	private TreeMap<String, LImage> imageMap;
	
	private NumberImageHandler() {
		loadResources();
	}
	
	public LImage getImage(String numberChar, String prefix) {
		String key = prefix + "/" + numberChar;
		if(!this.imageMap.containsKey(key)) {
			LImage image = new LImage(key + ".png");
			this.imageMap.put(numberChar, image);
		}
		return this.imageMap.get(numberChar);
	}
	
	public void loadResources() {
		this.imageMap = new TreeMap<String, LImage>();
	}
}

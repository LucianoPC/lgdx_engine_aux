package lgdx_engine_aux.util;

import com.badlogic.gdx.Gdx;

public class ScreenParams {

	public static final String IMAGE_MDPI_SIZE = "mdpi";
	public static final String IMAGE_HDPI_SIZE = "hdpi";
	public static final String IMAGE_XHDPI_SIZE = "xhdpi";
	public static final String IMAGE_XXHDPI_SIZE = "xxhdpi";
	
	public static float BORDER = 5.0f;
	public static float MULTIPLIER = 1.0f;
	
	private static ScreenParams instance;

	public static ScreenParams getInstance() {
		if (instance == null) {
			instance = new ScreenParams();
		}
		
		return instance;
	}
	
	public int width;
	public int height;
	public String imageFolder;
	
	private ScreenParams() {
		loadResourses();
	}
	
	public void loadResourses() {
		defineDimensions();
	}
	
	private void defineDimensions() {
		int gdxWidth = Gdx.graphics.getWidth();
		int gdxHeight = Gdx.graphics.getHeight();
		
		this.width = gdxWidth;
		this.height = gdxHeight;
		
		this.imageFolder = IMAGE_MDPI_SIZE;
		
		float screenMultiplier = 1.0f;
		if(gdxWidth >= 320) {
			screenMultiplier = 1.0f;
		}
		if(gdxWidth >= 480) {
			screenMultiplier = 1.5f;
		}
		if(gdxWidth >= 720) {
			screenMultiplier = 2.0f;
		} else if(gdxWidth >= 1080) {
			screenMultiplier = 3.0f;
		}
		
		float dpiMultiplier = Gdx.graphics.getDensity();
		ScreenParams.MULTIPLIER = Math.max(screenMultiplier, dpiMultiplier);
		
		if(ScreenParams.MULTIPLIER >= 1.5f) {
			this.imageFolder = IMAGE_HDPI_SIZE;
		}
		if(ScreenParams.MULTIPLIER >= 2.0f) {
			this.imageFolder = IMAGE_XHDPI_SIZE;
		} 
		if(ScreenParams.MULTIPLIER >= 3.0f) {
			this.imageFolder = IMAGE_XXHDPI_SIZE;
		}
		
		ScreenParams.BORDER *= ScreenParams.MULTIPLIER;
//		this.height = (int) (gdxHeight / (gdxWidth / this.width));
//		this.height = 800;
		
//		Gdx.app.log("debug", "width: " + this.width);
//		Gdx.app.log("debug", "height: " + this.height);
//		Gdx.app.log("debug", "gdxWidth: " + gdxWidth);
//		Gdx.app.log("debug", "gdxHeight: " + gdxHeight);
//		Gdx.app.log("debug", "imageFolder: " + this.imageFolder);
		
//		Gdx.app.log("debug", "DPI: " + Gdx.graphics.getDensity());
	}

}

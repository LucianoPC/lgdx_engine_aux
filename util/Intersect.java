package lgdx_engine_aux.util;

import lgdx_engine_aux.basic.Positionable;
import lgdx_engine_aux.handlers.PositionHandler;

public class Intersect {

	public static boolean rect(Positionable positionable, float x, float y) {
		return rect(positionable, 0.0f, x, y);
	}

	public static boolean rect(Positionable positionable, float fakeArea,
			float x, float y) {
		float left = PositionHandler.getLeft(positionable) - fakeArea;
		float right = PositionHandler.getRight(positionable) + fakeArea;
		float top = PositionHandler.getTop(positionable) + fakeArea;
		float bottom = PositionHandler.getBottom(positionable) - fakeArea;

		return (x >= left) && (x <= right) && (y >= bottom) && (y <= top);
	}

	public static boolean point(Positionable a, Positionable b) {
		return (Math.abs(a.getPosition().x - b.getPosition().x) < 1.0f)
				&& (Math.abs(a.getPosition().y - b.getPosition().y) < 1.0f);
	}
}

package lgdx_engine_aux.basic;

import java.util.LinkedList;

public class GameScreenMachine {

	public interface GameScreenMachineListener {
		public void onGameScreenChange(GameScreen gameScreen);
	}

	private static GameScreenMachine instance;

	public static GameScreenMachine getInstance() {
		if (instance == null) {
			instance = new GameScreenMachine();
		}
		return instance;
	}

	private LinkedList<GameScreen> gameScreenList;
	private GameScreenMachineListener gameScreenMachineListener;

	public GameScreenMachine() {
		this.gameScreenList = new LinkedList<GameScreen>();
	}

	public void push(GameScreen gameScreen) {
		this.gameScreenList.push(gameScreen);
		notifyOnGameScreenChange();
		gameScreen.onResume();
	}

	public void pop() {
		if (!this.gameScreenList.isEmpty()) {
			this.gameScreenList.pop();
			if (!this.gameScreenList.isEmpty()) {
				this.gameScreenList.getFirst().onResume();
			}
			notifyOnGameScreenChange();
		}
	}

	public GameScreen top() {
		return this.gameScreenList.getFirst();
	}
	
	public void clear() {
		this.gameScreenList.clear();
	}

	private void notifyOnGameScreenChange() {
		if (canNotifyListener()) {
			this.gameScreenMachineListener.onGameScreenChange(top());
		}
	}

	private boolean canNotifyListener() {
		return haveListener() && !this.gameScreenList.isEmpty();
	}

	private boolean haveListener() {
		return this.gameScreenMachineListener != null;
	}

	public void setGameScreenMachineListener(
			GameScreenMachineListener gameScreenMachineListener) {
		this.gameScreenMachineListener = gameScreenMachineListener;
	}
}

package lgdx_engine_aux.basic;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

public class MoveImage implements Positionable {
	
	public interface MoveImageListener {
		public void onFinishMove(Positionable positionable);
	}

	private Positionable positionable;

	private Vector2 velocity;
	private Vector2 position;
	private Vector2 imagePosition;

	private boolean run;
	private boolean finishMove;
	private float scalarVelocity;
	
	private MoveImageListener listener;

	public MoveImage(Positionable positionable, float velocity) {
		setPositionable(positionable, velocity);
	}

	public void run() {
		this.run = true;
		this.finishMove = false;
		
		startVelocity();
		this.positionable.getPosition().set(this.position);
	}

	public void setPositionable(Positionable positionable) {
		setPositionable(positionable, this.scalarVelocity);
	}

	public void setPositionable(Positionable positionable, float velocity) {
		this.positionable = positionable;
		this.imagePosition = positionable.getPosition();

		this.run = false;
		this.finishMove = false;
		this.scalarVelocity = velocity;
		this.velocity = new Vector2();
		this.position = new Vector2();
		this.imagePosition = new Vector2();
		this.imagePosition.set(positionable.getPosition());
	}

	@Override
	public void update(float delta) {
		if (this.run) {
			if (this.position.dst(this.imagePosition) > this.scalarVelocity) {
				if (!this.finishMove) {
					this.position.add(this.velocity);
					this.positionable.getPosition().set(this.position);
				}
			}
			if (this.position.dst(this.imagePosition) <= this.scalarVelocity) {
				if (!this.finishMove) {
					this.position.set(this.imagePosition);
					this.positionable.getPosition().set(this.position);
					
					this.run = false;
					this.finishMove = true;
					notifyOnFinishMove();
				}
			}
		}
	}

	@Override
	public void render(SpriteBatch spriteBatch, float runTime) {

	}

	@Override
	public Vector2 getPosition() {
		return this.position;
	}

	@Override
	public int getWidth() {
		return this.positionable.getWidth();
	}

	@Override
	public int getHeight() {
		return this.positionable.getHeight();
	}
	
	public Positionable getPositionable() {
		return this.positionable;
	}

	public void setMoveImageListener(MoveImageListener listener) {
		this.listener = listener;
	}
	
	private void startVelocity() {
		this.velocity.set(this.imagePosition);
		this.velocity.sub(this.position);
		this.velocity.nor();
		this.velocity.scl(this.scalarVelocity);
	}
	
	private void notifyOnFinishMove() {
		if (haveListener()) {
			this.listener.onFinishMove(this.positionable);
		}
	}
	
	private boolean haveListener() {
		return this.listener != null;
	}
}

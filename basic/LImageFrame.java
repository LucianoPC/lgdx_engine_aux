package lgdx_engine_aux.basic;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class LImageFrame extends LImage {

	private int cols;
	private int indexCol;
	private TextureRegion textureRegions[];

	public LImageFrame(String imagePath, int cols) {
		super(imagePath);
		this.cols = cols;
		this.indexCol = 0;

		int x, y = 0;
		int width = getWidth();
		int height = getHeight();
		Texture texture = getTexture();

		this.textureRegions = new TextureRegion[cols];
		for (int i = 0; i < cols; i++) {
			x = i * width;
			this.textureRegions[i] = new TextureRegion(texture, x, y, width,
					height);
		}
	}

	public void setCol(int indexCol) {
		this.indexCol = indexCol;
	}
	
	public void incrementCol() {
		this.indexCol++;
		this.indexCol = this.indexCol % this.cols;
	}

	@Override
	public void render(SpriteBatch spriteBatch, float runTime) {
		if (!isVisible()) {
			return;
		}
		int halfWidth = getWidth() / 2;
		int halfHeight = getHeight() / 2;

		TextureRegion textureRegion = this.textureRegions[this.indexCol];
		spriteBatch.draw(textureRegion, this.position.x - halfWidth,
				this.position.y - halfHeight);
	}

	@Override
	public int getWidth() {
		return super.getWidth() / this.cols;
	}
}

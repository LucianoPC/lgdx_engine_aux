package lgdx_engine_aux.basic;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class SleepTimer implements GameObject {

	public interface SleepTimerListener {
		public void onTimeFinish(SleepTimer sleepTimer);
	}
	
	private float time;
	private SleepTimerListener listener;

	public SleepTimer() {
		this.time = 0.0f;
	}

	public void start(float time) {
		this.time = time;
	}

	@Override
	public void update(float delta) {
		if (this.time > 0.0f) {
			this.time -= delta;
			
			if(this.time <= 0.0f) {
				this.time = 0.0f;
				notifyOnFinishMove();
			}
		}
	}

	@Override
	public void render(SpriteBatch spriteBatch, float runTime) {

	}
	
	public void setSleepTimerListener(SleepTimerListener listener) {
		this.listener = listener;
	}
	
	private void notifyOnFinishMove() {
		if (haveListener()) {
			this.listener.onTimeFinish(this);
		}
	}
	
	private boolean haveListener() {
		return this.listener != null;
	}
}

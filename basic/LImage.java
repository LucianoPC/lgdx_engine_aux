package lgdx_engine_aux.basic;

import lgdx_engine_aux.handlers.ImageHandler;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

public class LImage implements Positionable {

	protected TextureRegion textureRegionAtlas;
	protected Vector2 position;
	protected Integer width;
	protected Integer height;
	protected Float angle;

	private float alpha;
	
	private int col;
	private int row;
	private int colsNumber;
	private int rowsNumber;

	private boolean visible;
	private String imagePath;

	private int textureRegionX;
	private int textureRegionY;

	public LImage(String imagePath) {
		this(imagePath, 1);
	}

	public LImage(String imagePath, int colsNumber) {
		this(imagePath, colsNumber, 1);
	}

	public LImage(String imagePath, int colsNumber, int rowsNumber) {
		this.alpha = 1.0f;
		this.col = 0;
		this.row = 0;
		this.colsNumber = colsNumber;
		this.rowsNumber = rowsNumber;
		this.visible = true;
		this.position = new Vector2();
		this.imagePath = imagePath;
		this.angle = 0.0f;

		this.textureRegionAtlas = ImageHandler.load(imagePath);
		this.width = this.textureRegionAtlas.getRegionWidth() / this.colsNumber;
		this.height = this.textureRegionAtlas.getRegionHeight()
				/ this.rowsNumber;
		this.textureRegionX = this.textureRegionAtlas.getRegionX();
		this.textureRegionY = this.textureRegionAtlas.getRegionY();
	}

	@Override
	public void render(SpriteBatch spriteBatch, float runTime) {
		if (!this.visible) {
			return;
		}
		int halfWidth = this.width / 2;
		int halfHeight = this.height / 2;

		float x = this.position.x - halfWidth;
		float y = this.position.y - halfHeight;
		
		Color color = spriteBatch.getColor();
		spriteBatch.setColor(color.r, color.g, color.b, this.alpha);
		
		spriteBatch.draw(this.textureRegionAtlas, x, y, halfWidth, halfHeight,
				this.width, this.height, 1, 1, this.angle);

		spriteBatch.setColor(color.r, color.g, color.b, 1.0f);
	}

	@Override
	public void update(float delta) {

	}
	
	private void onChangeImageFrame() {
		this.textureRegionAtlas.setRegion(this.textureRegionX
				+ (this.width * this.col), this.textureRegionY
				+ (this.height * this.row), this.width, this.height);
	}

	public Vector2 getPosition() {
		return this.position;
	}

	public Float getX() {
		return this.position.x;
	}

	public Float getY() {
		return this.position.y;
	}

	public int getWidth() {
		return this.width;
	}

	public int getHeight() {
		return this.height;
	}

	public Texture getTexture() {
		return this.textureRegionAtlas.getTexture();
	}

	public boolean isVisible() {
		return this.visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	public String getImagePath() {
		return this.imagePath;
	}

	public Float getAngle() {
		return angle;
	}

	public void setAngle(Float angle) {
		this.angle = angle;
	}

	public int getCol() {
		return this.col;
	}

	public void setCol(int col) {
		this.col = col;
		onChangeImageFrame();
	}

	public void incrementCol() {
		this.col++;
		onChangeImageFrame();
	}
	
	public int getColNumber() {
		return this.colsNumber;
	}

	public void incrementRow() {
		this.row++;
		onChangeImageFrame();
	}

	public void setRow(int row) {
		this.row = row;
		onChangeImageFrame();
	}

	public int getRow() {
		return this.row;
	}

	public int getRowNumber() {
		return this.rowsNumber;
	}

	public float getAlpha() {
		return alpha;
	}

	public void setAlpha(float alpha) {
		this.alpha = alpha;
	}
}

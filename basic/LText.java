package lgdx_engine_aux.basic;

import lgdx_engine_aux.handlers.FontHandler;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

public class LText implements GameObject, Positionable {

	private int width;
	private int height;

	private int size;
	private String text;
	private Vector2 position;

	private BitmapFont bitmapFont;
	private GlyphLayout glyphLayout;

	public LText() {
		this("", FontHandler.MEDIUM_SIZE);
	}
	
	public LText(int size) {
		this("", size);
	}
	
	public LText(String text) {
		this(text, FontHandler.MEDIUM_SIZE);
	}

	public LText(String text, int size) {
		this.text = text;
		this.size = size;
		initializeFont();
		initializeAttributes(text, size);
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
		updateGlyphLayout();
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
		initializeFont();
	}

	@Override
	public void update(float delta) {
	}

	@Override
	public void render(SpriteBatch spriteBatch, float runTime) {
		float x = this.position.x - (getWidth() / 2);
		float y = this.position.y + (getHeight() / 2);
		this.bitmapFont.draw(spriteBatch, this.glyphLayout, x, y);
	}

	@Override
	public Vector2 getPosition() {
		return this.position;
	}

	@Override
	public int getWidth() {
		updateGlyphLayout();
		return (int) this.width;
	}

	@Override
	public int getHeight() {
		updateGlyphLayout();
		return (int) this.height;
	}

	private void initializeFont() {
		this.bitmapFont = FontHandler.getInstance().getBitmapFont(this.size);

		this.glyphLayout = new GlyphLayout();
		updateGlyphLayout();
	}

	private void initializeAttributes(String text, int size) {
		setText(text);
		this.position = new Vector2();
	}

	private void updateGlyphLayout() {
		this.glyphLayout.setText(this.bitmapFont, this.text);
		this.width = (int) this.glyphLayout.width;
		this.height = (int) this.glyphLayout.height;
	}
}

package lgdx_engine_aux.basic;

import lgdx_engine_aux.util.Intersect;

public class RectButton extends LImage implements Touchable {

	public interface RectButtonListener {
		public void onRectButtonTouchDown(RectButton rectButton);
	}

	private float fakeArea;
	private boolean enable;
	private RectButtonListener listener;

	public RectButton(String imagePath) {
		this(imagePath, 1, 1);
	}

	public RectButton(String imagePath, int colsNumber, int rowsNumber) {
		super(imagePath, colsNumber, rowsNumber);

		this.enable = true;
		this.fakeArea = 0.0f;
	}

	@Override
	public boolean touchDown(float x, float y) {
		if (this.enable && isTouchMe(x, y)) {
			notifyOnRectButtonTouchDown();
			return true;
		}
		return false;
	}

	public boolean isEnable() {
		return enable;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}

	public void setRectButtonListener(RectButtonListener listener) {
		this.listener = listener;
	}

	public float getFakeArea() {
		return fakeArea;
	}

	public void setFakeArea(float fakeArea) {
		this.fakeArea = fakeArea;
	}

	private boolean isTouchMe(float x, float y) {
		return this.enable && Intersect.rect(this, this.fakeArea, x, y);
	}

	private void notifyOnRectButtonTouchDown() {
		if (haveListener()) {
			this.listener.onRectButtonTouchDown(this);
		}
	}

	private boolean haveListener() {
		return this.listener != null;
	}
}

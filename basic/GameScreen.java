package lgdx_engine_aux.basic;

import java.util.ArrayList;

import lgdx_engine_aux.util.ScreenParams;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

public class GameScreen implements Screen, Positionable {

	public static final int NUMBER_OF_GAME_OBJECT_LAYERS = 2;

	public static int WIDTH;
	public static int HEIGHT;

	private float runTime;
	private boolean enableTouch;

	private Vector2 position;

	private SpriteBatch spriteBatch;
	private OrthographicCamera camera;

	private ArrayList<GameObject> gameObjectList;
	private ArrayList<ArrayList<GameObject>> gameObjectLayers;

	private Color bgColor;
	
	private GameScreenInputProcessor inputProcessor;

	public GameScreen() {
		this.runTime = 0.0f;
		this.enableTouch = true;
		this.bgColor = new Color(1.0f, 1.0f, 1.0f, 1.0f);
		initializeStaticAttributes();
		initializeCameraAndSpriteBatch();
		initializeGameObjectList();
		
		this.inputProcessor = new GameScreenInputProcessor();
		Gdx.input.setInputProcessor(this.inputProcessor);
	}

	public void onResume() {
		Gdx.input.setInputProcessor(this.inputProcessor);
	}

	public void addGameObject(GameObject gameObject) {
		addGameObject(gameObject, 0);
	}

	public void addGameObject(GameObject gameObject, int renderLayer) {
		if (!this.gameObjectLayers.get(renderLayer).contains(gameObject)) {
			this.gameObjectList.add(gameObject);
			this.gameObjectLayers.get(renderLayer).add(gameObject);
		}
	}

	public void removeGameObject(GameObject gameObject) {
		this.gameObjectList.remove(gameObject);
		for (int i = 0; i < this.gameObjectLayers.size(); i++) {
			this.gameObjectLayers.get(i).remove(gameObject);
		}
	}

	public void setGameObjectLayer(GameObject gameObject, int renderLayer) {
		if (this.gameObjectList.contains(gameObject)) {
			removeGameObject(gameObject);
			addGameObject(gameObject, renderLayer);
		}
	}

	@Override
	public void render(float delta) {
//		Gdx.app.log("debug", "delta: " + delta);
//		Gdx.app.log("debug", "Java Heap: " + Gdx.app.getJavaHeap());
//		Gdx.app.log("debug", "Native Heap: " + Gdx.app.getNativeHeap());
		update(delta);
		renderSpriteBatch();
		this.runTime += delta;
	}

	public void update(float delta) {
		for (int i = 0; i < this.gameObjectList.size(); i++) {
			this.gameObjectList.get(i).update(delta);
		}
	}

	public void renderObjects(SpriteBatch spriteBatch, float runtTime) {
		for (int i = 0; i < this.gameObjectLayers.size(); i++) {
			for (int j = 0; j < this.gameObjectLayers.get(i).size(); j++) {
				this.gameObjectLayers.get(i).get(j)
						.render(spriteBatch, runTime);
			}
		}
	}

	@Override
	public void render(SpriteBatch spriteBatch, float runTime) {
	}

	private void initializeStaticAttributes() {
		WIDTH = ScreenParams.getInstance().width;
		HEIGHT = ScreenParams.getInstance().height;
	}

	private void initializeCameraAndSpriteBatch() {
		this.position = new Vector2(WIDTH / 2, HEIGHT / 2);

		this.camera = new OrthographicCamera();
		this.camera.setToOrtho(false, WIDTH, HEIGHT);

		this.spriteBatch = new SpriteBatch();
		this.spriteBatch.setProjectionMatrix(this.camera.combined);
	}

	private void initializeGameObjectList() {
		this.gameObjectList = new ArrayList<GameObject>();

		this.gameObjectLayers = new ArrayList<ArrayList<GameObject>>();
		for (int i = 0; i < NUMBER_OF_GAME_OBJECT_LAYERS; i++) {
			this.gameObjectLayers.add(new ArrayList<GameObject>());
		}
	}

	private void renderSpriteBatch() {
		Gdx.gl.glClearColor(this.bgColor.r, this.bgColor.g, this.bgColor.b,
				this.bgColor.a);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		this.spriteBatch.begin();
		renderObjects(this.spriteBatch, this.runTime);
		this.spriteBatch.end();
	}

	public void setBackgroundColor(Color bgColor) {
		this.bgColor = bgColor;
	}

	@Override
	public void show() {
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void hide() {
	}

	@Override
	public void dispose() {
	}

	public int getWidth() {
		return WIDTH;
	}

	public int getHeight() {
		return HEIGHT;
	}

	@Override
	public Vector2 getPosition() {
		return this.position;
	}

	public ArrayList<GameObject> getGameObjects() {
		return this.gameObjectList;
	}
	
	public boolean isEnableTouch() {
		return enableTouch;
	}

	public void setEnableTouch(boolean enableTouch) {
		this.enableTouch = enableTouch;
	}

	private class GameScreenInputProcessor extends BasicInputProcessor {
		
		@Override
		public boolean touchDown(int screenX, int screenY, int p, int b) {

			int x = screenX * GameScreen.WIDTH / Gdx.graphics.getWidth();
			int y = GameScreen.HEIGHT
					- (screenY * GameScreen.HEIGHT / Gdx.graphics.getHeight());
			
			if (!enableTouch) {
				return false;
			}

			for (int i = 0; i < gameObjectList.size(); i++) {
				if(gameObjectList.get(i) instanceof Touchable) {
					Touchable touchable = (Touchable) gameObjectList.get(i);
					if(touchable.touchDown(x, y)) {
						return true;
					}
				}
			}
			
			return false;
		}
	}
}

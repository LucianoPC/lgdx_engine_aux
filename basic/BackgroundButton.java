package lgdx_engine_aux.basic;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class BackgroundButton extends RectButton {

	private LImage image;

	public BackgroundButton(String backgroundPath, String imagePath) {
		super(backgroundPath);

		this.image = new LImage(imagePath);
	}

	@Override
	public void update(float delta) {
		super.update(delta);

		if (this.image.getPosition() != this.getPosition()) {
			this.image.getPosition().x = this.getPosition().x;
			this.image.getPosition().y = this.getPosition().y;
		}
	}

	@Override
	public void render(SpriteBatch spriteBatch, float runTime) {
		super.render(spriteBatch, runTime);

		this.image.render(spriteBatch, runTime);
	}
	
	@Override
	public String getImagePath() {
		return this.image.getImagePath();
	}
	
	@Override
	public void setVisible(boolean visible) {
		super.setVisible(visible);
		this.image.setVisible(visible);
	}
}

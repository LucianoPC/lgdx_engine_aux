package lgdx_engine_aux.basic;

public interface Touchable extends Positionable {

	public boolean touchDown(float x, float y);
}

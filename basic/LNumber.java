package lgdx_engine_aux.basic;

import java.util.ArrayList;

import lgdx_engine_aux.handlers.NumberImageHandler;
import lgdx_engine_aux.handlers.PositionHandler;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

public class LNumber implements GameObject, Positionable {
	
	public static final String SMALL = "";
	public static final String MEDIUM = "mid_";

	private int width;
	private int height;
	private String size;
	private String prefix;
	private boolean visible;
	private Vector2 position;
	private ArrayList<LImage> imageList;
	
	public LNumber(int number, String prefix) {
		this(String.valueOf(number), prefix);
	}

	public LNumber(String numberText, String prefix) {
		this.prefix = prefix;
		this.visible = true;
		this.position = new Vector2();
		this.imageList = new ArrayList<LImage>();
		this.size = SMALL;
		setText(numberText);
	}

	public void setText(String numberText) {
		updateImageList(numberText);
		updateWidth();
		updateHeight();
		updateNumbersPosition();
	}
	
	public void setSize(String size) {
		this.size = size;
	}

	public void updateNumbersPosition() {
		LImage image;

		image = this.imageList.get(0);
		PositionHandler.alignLeftToLeft(this, image);

		for (int i = 1; i < this.imageList.size(); i++) {
			image = this.imageList.get(i);
			PositionHandler.alignLeftToRight(this, image, 1.0f);
		}

		for (int i = 0; i < this.imageList.size(); i++) {
			image = this.imageList.get(i);
			PositionHandler.alignCenterVertical(this, image);
		}
	}

	@Override
	public void update(float delta) {
		for (int i = 0; i < this.imageList.size(); i++) {
			this.imageList.get(i).update(delta);
		}
	}

	@Override
	public void render(SpriteBatch spriteBatch, float runTime) {
		if(!this.visible) {
			return;
		}
		for (int i = 0; i < this.imageList.size(); i++) {
			updateNumberPositionByIndex(i);
			this.imageList.get(i).render(spriteBatch, runTime);
		}
	}

	@Override
	public Vector2 getPosition() {
		return this.position;
	}

	@Override
	public int getWidth() {
		return this.width;
	}

	@Override
	public int getHeight() {
		return this.height;
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	private void updateImageList(String numberText) {
		this.imageList.clear();

		LImage image;
		char numberChar;
		NumberImageHandler numberHandler = NumberImageHandler.getInstance();
		for (int i = 0; i < numberText.length(); i++) {
			numberChar = numberText.charAt(i);
			image = numberHandler.getImage(this.size + numberChar, prefix);
			this.imageList.add(image);
		}
	}

	private void updateWidth() {
		this.width = 0;
		if (!this.imageList.isEmpty()) {
			this.width += this.imageList.size() - 1;
			for (int i = 0; i < this.imageList.size(); i++) {
				this.width += this.imageList.get(i).getWidth();
			}
		}
	}

	private void updateHeight() {
		this.height = 0;

		if (!this.imageList.isEmpty()) {
			this.height = this.imageList.get(0).getHeight();
		}
	}

	private void updateNumberPositionByIndex(int index) {
		LImage image = this.imageList.get(index);

		if (index == 0) {
			PositionHandler.alignLeftToLeft(this, image);
		} else {
			LImage imagePrevious = this.imageList.get(index - 1);
			PositionHandler.alignLeftToRight(imagePrevious, image, 1.0f);
		}
		PositionHandler.alignCenterVertical(this, image);
	}
}

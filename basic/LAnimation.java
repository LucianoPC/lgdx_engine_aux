package lgdx_engine_aux.basic;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class LAnimation implements GameObject {
	
	public interface LAnimationListener {
		public void onAnimationFinish(LImage image);
	}

	private static final float FRAME_SPEED_DEFAULT = 0.25f;

	private float time;
	private float deltaTime;
	private float animationTime;

	private boolean started;

	private LImage image;
	
	private LAnimationListener listener;

	public LAnimation(LImage image) {
		this(image, FRAME_SPEED_DEFAULT);
	}

	public LAnimation(LImage image, float animationTime) {
		this.image = image;
		this.started = false;
		this.animationTime = animationTime;
	}

	@Override
	public void update(float delta) {
		if (this.started) {
			this.time += delta;

			if (this.image.getCol() < this.image.getColNumber() - 1) {
				while (this.time >= this.deltaTime) {
					this.time -= this.deltaTime;
					this.image.incrementCol();
				}
				if (this.image.getCol() >= this.image.getColNumber() - 1) {
					this.image.setCol(this.image.getColNumber() - 1);
				}
			} else {
				this.started = false;
				this.image.setCol(this.image.getColNumber() - 1);
				notifyOnAnimationFinish();
			}
		}
	}

	@Override
	public void render(SpriteBatch spriteBatch, float runTime) {

	}

	public void startAnimation() {
		this.image.setCol(0);
		this.time = 0L;
		this.started = true;
		this.deltaTime = this.animationTime / this.image.getColNumber();
	}

	public void setTime(float animationTime) {
		this.animationTime = animationTime;
	}
	
	private void notifyOnAnimationFinish() {
		if (haveListener()) {
			this.listener.onAnimationFinish(this.image);
		}
	}

	private boolean haveListener() {
		return this.listener != null;
	}

	public void setLAnimationListener(LAnimationListener listener) {
		this.listener = listener;
	}
}

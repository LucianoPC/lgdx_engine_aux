package lgdx_engine_aux.basic;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public interface GameObject {

	public void update(float delta);

	public void render(SpriteBatch spriteBatch, float runTime);
}

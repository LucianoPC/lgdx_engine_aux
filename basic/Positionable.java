package lgdx_engine_aux.basic;

import com.badlogic.gdx.math.Vector2;

public interface Positionable extends GameObject {

	public Vector2 getPosition();

	public int getWidth();

	public int getHeight();
}

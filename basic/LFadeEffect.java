package lgdx_engine_aux.basic;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class LFadeEffect implements GameObject {
	
	public interface LFadeEffectListener {
		public void onFadeFinish(LImage image);
	}
	
	private float time;
	private float velociry;
	private float animationTime;

	private boolean started;

	private LImage image;
	
	private LFadeEffectListener listener;
	
	public LFadeEffect(LImage image, float animationTime) {
		this.image = image;
		this.started = false;
		this.animationTime = animationTime;
	}
	
	@Override
	public void update(float delta) {
		if (this.started) {
			this.time += delta;

			this.image.setAlpha(this.time * this.velociry);
			if (this.image.getAlpha() > 1.0f) {
				this.started = false;
				this.image.setAlpha(1.0f);
				notifyOnFadeFinish();
			}
		}
	}

	@Override
	public void render(SpriteBatch spriteBatch, float runTime) {
		
	}

	public void startFade() {
		this.image.setAlpha(0.0f);
		this.time = 0L;
		this.started = true;
		this.velociry = 1.0f / this.animationTime;
	}

	public void setTime(long animationTime) {
		this.animationTime = animationTime;
	}
	
	private void notifyOnFadeFinish() {
		if (haveListener()) {
			this.listener.onFadeFinish(this.image);
		}
	}

	private boolean haveListener() {
		return this.listener != null;
	}

	public void setLFadeEffectListener(LFadeEffectListener listener) {
		this.listener = listener;
	}
}

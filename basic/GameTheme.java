package lgdx_engine_aux.basic;

import com.badlogic.gdx.graphics.Color;

public abstract class GameTheme {

	private Color lightColor;
	private Color mediumColor;
	private Color strongColor;

	public GameTheme() {
		this.lightColor = initLightColor();
		this.mediumColor = initMediumColor();
		this.strongColor = initStrongColor();
	}

	public abstract String getFolder();

	protected abstract Color initLightColor();

	protected abstract Color initMediumColor();

	protected abstract Color initStrongColor();

	public Color getLightColor() {
		return lightColor;
	}

	public Color getMediumColor() {
		return mediumColor;
	}

	public Color getStrongColor() {
		return strongColor;
	}
}
